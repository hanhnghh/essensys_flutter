class Dimension {
  static const double pad10 = 10.0;
  static const double pad20 = 20.0;
  static const double pad37 = 37.0;
  static const double pad22 = 22.0;
  static const double pad16 = 16.0;
  static const double pad5 = 5.0;
  static const double pad8 = 8.0;
  static const double pad14 = 14.0;
  static const double pad120 = 120.0;
  static const double pad6 = 6.0;
  static const double pad80 = 80.0;
  static const double pad100 = 100.0;

}
