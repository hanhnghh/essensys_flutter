import 'package:essen_flutter/core/bloc/base_bloc_component.dart';
import 'package:essen_flutter/core/event/observable.dart';
import 'package:intl/intl.dart';

import 'childSelectionModel.dart';

class ChildSelectionBloc extends BaseBloc {
  List childList;
  ObservableField<List<dynamic>> childListObserField;

  ChildSelectionBloc() {
    childList = List.generate(
        2,
        (index) => ChildInfoModel(index.toString(), "Baby$index",
            DateFormat("dd/MM/yyy").format(DateTime.now())));
    childList.add("add new child");
    childListObserField = ObservableField(childList);
  }

  @override
  void dispose() {
    childListObserField.close();
    super.dispose();
  }
}
