class ChildInfoModel {
  String childName;
  String dateOfBirth;
  String id;
  String imageUrl;
  ChildInfoModel(this.id, this.childName, this.dateOfBirth,
      {this.imageUrl = ""});
}

enum Gender { Male, Female }
