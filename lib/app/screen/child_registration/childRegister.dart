import 'package:flutter/cupertino.dart';

import '../../../app/common/theme/dimens.dart';
import '../../../app/common/theme/styles.dart';
import '../../../app/screen/child_registration/childRegisterBloc.dart';
import '../../../core/core.dart';
import 'package:flutter/material.dart';

class ChildRegisterPage extends BasePage<ChildRegisterBloc> {
  ChildRegisterPage({NavigatorState navigator}) : super(navigator: navigator);
  @override
  ChildRegisterBloc getBlocData(BuildContext context) {
    return BlocProviders.of<ChildRegisterBloc>(context);
  }

  @override
  void onInitEvent(BuildContext context) {}

  @override
  Widget onPageBuild(BuildContext context) {
    return Scaffold(
      body: Container(
          width: MediaQuery.of(context).size.width,
          padding:
              EdgeInsets.fromLTRB(Dimension.pad20, 0.0, Dimension.pad20, 0.0),
          decoration: BoxDecoration(
              gradient: Styles.screenBackgroundGradient,
              shape: BoxShape.rectangle),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: Dimension.pad37),
              GestureDetector(
                onTap: () {
                  getNavigator(context).pop();
                },
                child: BackButton(),
              ),
              SizedBox(height: Dimension.pad20),
              Text("Child Register",
                  style: Theme.of(context).textTheme.headline)
            ],
          )),
    );
  }
}
