class AssetsImage {
  static String getImageUrl(String name) {
    return 'assets/images/$name';
  }
}
