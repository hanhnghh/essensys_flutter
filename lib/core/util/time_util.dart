import 'package:intl/intl.dart';
import '../util/string_util.dart';

class DateTimeUtils {
  static DateTimeUtils _instance;

  static DateTimeUtils get instance {
    if (_instance == null) _instance = DateTimeUtils();
    return _instance;
  }

  String convertDatetime(DateTime input,
      {String pattern = 'YYYY:mm:dd HH:mm:ss'}) {
    DateFormat dateFormat = DateFormat(pattern);

    try {
      return dateFormat.format(input);
    } on Exception catch (e) {
      print(e);
      return '';
    }
  }

  DateTime textToDatetime(String input) {
    return DateTime.tryParse(input);
  }

  bool isDatetimeType(String input) {
    return !StringUtil.isEmptyTrim(input) && DateTime.tryParse(input) != null;
  }

  bool isAfter(String datetime, String datetimeCompare) {
    try {
      DateFormat dateFormat = DateFormat('yyyy-MM-dd');
      DateTime date = dateFormat.parse(datetime);
      DateTime dateCompare = dateFormat.parse(datetimeCompare);

      return date.compareTo(dateCompare) >= 0;
    } on Exception catch (e) {
      print(e);
      return false;
    }
  }
}
