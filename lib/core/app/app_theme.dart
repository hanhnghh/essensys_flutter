import 'package:flutter/material.dart';

class AppTheme {
  ThemeData _themeData;
  static final AppTheme _instance = AppTheme.internal();

  AppTheme.internal() {
    _themeData = ThemeData(
        primaryColor: Color.fromRGBO(0, 198, 255, 1.0),
        primaryColorDark: Colors.green[600],
        primarySwatch: Colors.blue,
        backgroundColor: Colors.white,
        buttonColor: Color.fromRGBO(0, 198, 255, 1.0),
        primaryIconTheme: IconThemeData(color: Colors.yellow[700]),
        primaryTextTheme: TextTheme(
            title: TextStyle(color: Colors.white),
            subtitle: TextStyle(color: Colors.white, fontSize: 14),
            body1: TextStyle(color: Colors.black, fontSize: 16),
            body2: TextStyle(
                color: Colors.yellow[700],
                fontWeight: FontWeight.bold,
                fontSize: 20),
            display2: TextStyle(
              color: Colors.black54,
              fontSize: 14,
            ),
            button:
                TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
        textTheme: TextTheme(title: TextStyle(color: Colors.white)),
        iconTheme: IconThemeData(color: Colors.white));
  }

  ThemeData get themeData => this._themeData;
  factory AppTheme() {
    return _instance;
  }
}
