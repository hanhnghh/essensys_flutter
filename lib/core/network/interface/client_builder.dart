import '../../../core/network/interface/http_service.dart';
import '../../../core/network/interface/intercepter.dart';

abstract class ClientBuilder {
  String _baseUrl;
  int _requestTimeout;
  Map<String, String> _authenticator;
  List<Intercepter> _intercepters;
  int _retryCount;

  ClientBuilder setBaseUrl(String url) {
    this._baseUrl = url;
    return this;
  }

  String get baseUrl => this._baseUrl;

  ClientBuilder setRequestTimeout(int timeout) {
    this._requestTimeout = timeout;
    return this;
  }

  int get requestTimeout => this._requestTimeout;

  ClientBuilder setAuthenticator(Map<String, String> authens) {
    this._authenticator = authens;
    return this;
  }

  Map<String, String> get authenticator => this._authenticator;

  ClientBuilder setIntercepters(List<Intercepter> intercepters) {
    this._intercepters = intercepters;
    return this;
  }

  List<Intercepter> get intercepters => this._intercepters;

  int get retryCount => this._retryCount;

  ClientBuilder setRetryCount(int time) {
    this._retryCount = time;
    return this;
  }

  HttpService build();
}
