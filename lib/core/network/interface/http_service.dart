import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';

import '../../../core/model/respond_model.dart';

typedef void OnComplete(Response model);
typedef void OnFailed(RespondModel model);

abstract class HttpService {
  Future<void> get(
    String url, {
    Map<String, String> header,
    Map<String, String> queries,
    List<String> path,
    @required OnComplete onComplete,
    @required OnFailed onFailed,
  });

  Future<void> post(
    String url, {
    Map<String, String> header,
    Encoding encoding,
    body,
    @required OnComplete onComplete,
    @required OnFailed onFailed,
  });

  Future<void> put(
    String url, {
    Map<String, String> header,
    Encoding encoding,
    body,
    @required OnComplete onComplete,
    @required OnFailed onFailed,
  });

  Future<void> delete(
    String url, {
    Map<String, String> header,
    List<String> path,
    Map<String, String> queries,
    @required OnComplete onComplete,
    @required OnFailed onFailed,
  });
}
