import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

class LocalPreferences {
  static Future<T> get<T>(String key, T defaultData) async {
    var preference = await SharedPreferences.getInstance();
    try {
      T result = preference.get(key);
      if (result == null) throw NullThrownError();

      return result;
    } catch (e) {
      return defaultData;
    }
  }

  static Future<bool> setBool(String key, bool data) async {
    var preference = await SharedPreferences.getInstance();
    return preference.setBool(key, data);
  }

  static Future<bool> setDouble(String key, double data) async {
    var preference = await SharedPreferences.getInstance();
    return preference.setDouble(key, data);
  }

  static Future<bool> setInt(String key, int data) async {
    var preference = await SharedPreferences.getInstance();
    return preference.setInt(key, data);
  }

  static Future<bool> setString(String key, String data) async {
    var preference = await SharedPreferences.getInstance();
    return preference.setString(key, data);
  }

  static Future<bool> setStringList(String key, List<String> data) async {
    var preference = await SharedPreferences.getInstance();
    return preference.setStringList(key, data);
  }

  static Future<bool> clearKey(String key) async {
    var preference = await SharedPreferences.getInstance();
    return preference.remove(key);
  }

  static Future<bool> clearStorage() async {
    var preference = await SharedPreferences.getInstance();
    return preference.clear();
  }

}
