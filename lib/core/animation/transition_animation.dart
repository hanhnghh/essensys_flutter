import 'package:flutter/material.dart';

class TransitionAnim {
  static Widget slideTransition(BuildContext context, Animation<double> topAnim,
      Animation<double> middleAnim, Widget animatedWidget) {
    return SlideTransition(
      position: new Tween<Offset>(
        begin: Offset(1.0, 0.0),
        end: Offset(0.0, 0.0),
      ).animate(topAnim),
      child: animatedWidget,
    );
  }

  static Widget presentTransition(
      BuildContext context,
      Animation<double> topAnim,
      Animation<double> middleAnim,
      Widget animatedWidget) {
    return SlideTransition(
      position: Tween<Offset>(begin: Offset(0.0, 0.2), end: Offset(0.0, 0.0))
          .animate(topAnim),
      child: FadeTransition(
        opacity: topAnim,
        child: animatedWidget,
      ),
    );
  }
}
