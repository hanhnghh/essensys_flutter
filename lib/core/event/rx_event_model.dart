class FlutEventModel {
  String _message;
  dynamic _data;

  FlutEventModel(this._message, this._data);

  get getMessage => this._message;
  set setMessage(String message) => this._message;

  get getData => this._data;
  set setData(dynamic data) => this._data;
}
