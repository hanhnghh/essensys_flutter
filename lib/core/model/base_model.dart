import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
abstract class BaseModel {

  Map<String,dynamic> toMap();

  BaseModel fromJson(Map<String,dynamic> map);
}
