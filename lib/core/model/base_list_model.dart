import 'package:flutter/material.dart';

class BaseListModel {
  dynamic value;
  String title;

  BaseListModel({this.value, @required this.title});
}
